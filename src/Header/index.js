import React from 'react';
import { Link } from "react-router-dom";


export default function Header() {
  

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="#">Navbar</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <Link className="nav-link" to="/Home">Flight list</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/Add">Add Flight</Link>
          </li>
        
        </ul>
        <ul className="navbar-nav mr-right"> 
           <li className="nav-item">
                <a>Logout</a>
          </li>
        </ul>
      </div>
    </nav>
  )
}