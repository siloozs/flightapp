import React, { useEffect , useState } from 'react';
import { inject, observer } from "mobx-react";
import { toJS } from 'mobx';
import { useInput } from '../Utils/';

const Home = props => {
  
  const { value: findDestinsationVal , bind:bindDestination   } = useInput('');
  const [flights, setFlights] = useState([]);


  const handleSubmit = (event) =>{
          event.preventDefault();
          
  }

  useEffect( ()=>{
    if(findDestinsationVal){
          const searchValue = findDestinsationVal.toLowerCase();
          setFlights( props.store.flights.filter(item =>  item.to.toLowerCase().includes(searchValue)  ) );
    }else{
      setFlights(props.store.flights);
    }   

  }, [findDestinsationVal])

  if(!props.store.flights.length){
        return <div className="alert alert-warning" role="alert">There are No flights...</div>;
  }
  
  return (
    <div className="container mt-5">
        <form onSubmit={handleSubmit}>
        <div className="form-group">
          <input {...bindDestination} className="form-control" id="from-flight" placeholder="Enter flight Destination" />
        </div>
        </form>


      <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">from</th>
              <th scope="col">to</th>
              <th scope="col">departureTime​</th>
              <th scope="col">landingTime</th>
              <th scope="col">price</th>
            </tr>
          </thead>
          <tbody>
            {
              flights.map((flight ,idx) =>(
                <tr key={idx} >
                <td>{idx}</td>
                <td>{flight.from}</td>
                <td>{flight.to}</td>
                <td>{flight.departtime}</td>
                <td>{flight.landingTime}</td>
                <td>{flight.price}</td>
              </tr>
              ))
            }
          </tbody>
      </table>
    </div>
  )
  
}

// export default Home;
export default inject('store')(observer(Home));