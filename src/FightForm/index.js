import React , { useEffect  } from 'react'
import { useInput } from '../Utils/';
import { inject } from "mobx-react";
import './FightFrom.css';

 const  FightForm = props => {
  const { value: from , bind:bindFrom , reset:restFrom  } = useInput('');
  const { value: to , bind:bindTo , reset:restTo  } = useInput('');
  
  const { value: departtime , bind:binddepartTime , reset:restdepartTime  } = useInput('');
  const { value: landingTime , bind:bindlandTime , reset:restlandTime  } = useInput('');
  const { value: price , bind:bindPrice , reset:restPrice  } = useInput('');
  

	const handleSubmit = (evt) => {
    evt.preventDefault();
    if(from !== "" && to !== "" && departtime !== "" && landingTime !== "" && price !== "" ){
      props.store.addflight({
        from,
        to,
        departtime,
        landingTime,
        price
      })
    }
}

useEffect( () => {
    return () =>{
          restPrice();
          restlandTime();
          restdepartTime();
          restTo();
          restFrom();
    }
}, [])

  
  return (
    <div className="fight-from-addon">
      <h4>Add a Flight</h4>
      <hr />
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="from-flight">From</label>
          <input className="form-control" id="from-flight" placeholder="Enter From" {...bindFrom} />
        </div>
        <div className="form-group">
          <label htmlFor="destination-flight">Destination</label>
          <input className="form-control" id="destination-flight"  placeholder="Enter destination" {...bindTo}/>
        </div>
        <div className="form-group">
          <label htmlFor="depart-flight">depart time</label>
          <input  className="form-control" id="depart-flight" type="datetime" placeholder="Enter destination" {...binddepartTime}/>
        </div>
        <div className="form-group">
          <label htmlFor="time-flight">landing Time</label>
          <input className="form-control" id="time-flight" type="time"  placeholder="Enter Time" {...bindlandTime}/>
        </div>
        <div className="form-group">
          <label htmlFor="price-flight">price</label>
          <input className="form-control" id="price-flight" type="number"  placeholder="Enter price" {...bindPrice}/>
        </div>


         <button className="btn btn-primary">Add Flight</button>
      </form>
    </div>
  )
}

export default inject('store')(FightForm);