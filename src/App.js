import './App.css';

import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Provider } from 'mobx-react'; 
import store from './Store';
import { inject, observer } from "mobx-react";
import Login from './Login'
import Home from './Home';
import Header from './Header'
import FightForm from './FightForm';


function App() {
  

  return (
    <div className="App">
     <Provider store={store}>
    <Router>
          <div className="container">
            {
            !store.isLoggedIn.status ? <Login />  : <Switch>
                <Route path="/Home" exact>
                      <Header/>
                      <Home />
                </Route>
                <Route path="/Add" exact>
                  <Header/>
                  <FightForm />
                </Route>
                <Route path="/">
                  <Login />
                </Route>
              </Switch>
            }
        </div>
          </Router>
      </Provider>  
    </div>
  );
}

export default observer(App);
