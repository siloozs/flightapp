import {observable, action,  decorate } from 'mobx';


class FlightStore{
           flights = []
           isLoggedIn = { status: false } 

        addflight(flightObj){
            this.flights.push(flightObj);
        }

        setLogin(status){
          this.isLoggedIn.status = true;
        }

}


decorate(FlightStore, {
  flights: observable,
  isLoggedIn: observable,
  addflight:action,
  setLogin: action
});


const store = new FlightStore;
export default store;