import React from 'react';
import { inject } from "mobx-react";
import { useInput } from '../Utils/';
import {  withRouter } from "react-router-dom";

const Login = props => {

	const { value: user , bind:bindUserName , reset:restUserName  } = useInput('');
	const { value: pass , bind:bindPassword , reset:restPass  } = useInput('');

	const handleSubmit = (evt) => {
		evt.preventDefault();
		if(user === "user" && pass === "password"){
					props.history.push({ pathname: '/Home',})
			    props.setLogin(true);
				  restPass();
				  restUserName();
				
		}
}

	return (
		<div className="vh-100 justify-content-center align-items-center d-flex">
			<div className="col-md-4 col-md-offset-4" id="login">
				<section id="inner-wrapper" className="login">
					<article>
						<form onSubmit={handleSubmit}>
							<div className="form-group">
								<div className="input-group">
									<span className="input-group-addon"><i className="fa fa-envelope"> </i></span>
									<input type="text" className="form-control" placeholder="UserName" {...bindUserName}/>
								</div>
							</div>
							<div className="form-group">
								<div className="input-group">
									<span className="input-group-addon"><i className="fa fa-key"> </i></span>
									<input type="password" className="form-control" placeholder="Password" {...bindPassword} />
								</div>
							</div>
							<button type="submit" className="btn btn-success btn-block">Login</button>
						</form>
					</article>
				</section>
			</div>
		</div>
	)
}


export default inject(stores => {
	return { 
			setLogin: stores.store.setLogin,
			isLoggedIn: stores.store.isLoggedIn
	 }   
 })( withRouter(Login) );